FROM manticoresearch/manticore:latest

COPY ./sphinxsearch/music_genre.csv /var/lib/manticore/music_genre.csv
COPY ./sphinxsearch/query_popularity.csv /var/lib/manticore/query_popularity.csv
COPY ./sphinxsearch/manticore.conf /etc/manticoresearch/manticore.conf

RUN awk -v OFS=, 'NR ==1 {print "ID", $0; next} {print (NR-1), $0}' /var/lib/manticore/music_genre.csv > /var/lib/manticore/musics.csv
RUN sed -i '1d' /var/lib/manticore/musics.csv

RUN awk -v OFS=, 'NR ==1 {print "ID", $0; next} {print (NR-1), $0}' /var/lib/manticore/query_popularity.csv > /var/lib/manticore/popularity.csv
RUN sed -i '1d' /var/lib/manticore/popularity.csv

# RUN awk -v OFS=, 'NR ==1 {print "ID", $0; next} {print (NR-1), $0}' /home/ayaal/search_history.csv > /home/ayaal/history.csv
# RUN sed -i '1d' /home/ayaal/history.csv