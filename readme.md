# Manticore

Installed by: https://github.com/manticoresoftware/docker

Exposed ports:
- 51.250.9.222:9306 for connections from a MySQL client
- 51.250.9.222:9308 for connections via HTTP


curl -X POST 'http://51.250.9.222:9308/sql' -d 'mode=raw&query=SELECT artist_name FROM musics LIMIT 50'